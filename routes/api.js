var express = require('express');
var router = express.Router();
var EventModel    = require('../libs/mongoose').EventModel;

router.get('/events', (req, res) => {
    return EventModel.find(function (err, articles) {
        if (!err) {
            return res.send(articles);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.post('/events', function(req, res) {
    var geo = [req.body.longitude, req.body.latitude];
    var event = new EventModel({
        title: req.body.title,
        description: req.body.description,
        geo:geo
    });

    event.save(function (err) {
        if (!err) {
            console.log("event created");
            return res.send({ status: 'OK', article:event });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            console.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

module.exports = router;