var mongoose    = require('mongoose');

mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;

db.on('error', (err) => console.log('connection error:', err.message));
db.once('open', () => console.log("Connected to DB!"));

var Schema = mongoose.Schema;

var Event = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    geo: {type:[Number], required:true, index: '2d'}
    date: { type: Date, default: Date.now }
});

var User = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    description: { type: String, required: true },
    events: [Event]
});

var EventModel = mongoose.model('Event', Event);

module.exports.EventModel = EventModel;